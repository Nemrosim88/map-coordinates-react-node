import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import LoginForm from "../components/UIComponents/LoginForm";

function RoutesParams() {
  return (
    <Router>
      <div>
        <LoginForm/>
        <ul>
          <li>
            <Link to="/map">Map</Link>
          </li>
          <li>
            <Link to="/reister">Zillow Group</Link>
          </li>
          <li>
            <Link to="/yahoo">Yahoo</Link>
          </li>
          <li>
            <Link to="/modus-create">Modus Create</Link>
          </li>
        </ul>

        <Route path="/:id" component={Child} />

        {/*
           It's possible to use regular expressions to control what param values should be matched.
              * "/order/asc"  - matched
              * "/order/desc" - matched
              * "/order/foo"  - not matched
        */}
        {/* <Route
          path="/order/:direction(asc|desc)"
          component={ComponentWithRegex}
        /> */}
      </div>
    </Router>
  );
}

function Child({ match }) {
  return (
    <div>
      <h3>ID: {match.params.id}</h3>
    </div>
  );
}

function ComponentWithRegex({ match }) {
  return (
    <div>
      <h3>Only asc/desc are allowed: {match.params.direction}</h3>
    </div>
  );
}

export default RoutesParams;