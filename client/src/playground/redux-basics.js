// node redux-basics.js

const redux = require("redux");
const createStore = redux.createStore;

const ititialState = {
  counter: 0
};

// Reducer -> strongly connected to the store. can update the state in the end
// принимает два аргумента
// state and the action
// and than should return one thing and the updated state

//                    Стандартная запись
//   |
//   v
// const rootReducer = (state, action) =>{

//                 Значение по умолчанию (если state undefined)
//   |
//   v
const rootReducer = (state = ititialState, action) => {
  if (action.type === "INC_COUNTER") {
    // state ++ не сработает -> imutable
    // нежно создать новый объект

    return {
        // копируем значения старого state, а после редактируем копию
      ...state,
      counter: state.counter + 1
    };
  }

  if (action.type === "ADD_COUNTER") {
    // state ++ не сработает -> imutable
    // нежно создать новый объект

    return {
        // копируем значения старого state, а после редактируем копию
      ...state,

    //                        ЗНАЧЕНИЕ с ACTION_VALUE
    //                              |
    //                              V
      counter: state.counter + action.value
    };
  }

  return state;
};

//Store
const store = createStore(rootReducer); // need to be initialized with reducer
console.log(store.getState());


// Subscription ->
store.subscribe(()=>{
  console.log("sUBSCRIPTION", store.getState());
})


//Dispatching Action

// принимает один аргумент -> action у которого должен быть "type" property
// type - uniqe identifier
store.dispatch({ type: "INC_COUNTER" });

// увеличить значение на 10
//                 TYPE             ACTION_VALUE
//                  |                   |
//                  V                   V
store.dispatch({ type: "ADD_COUNTER", value: 10 });

console.log(store.getState());


