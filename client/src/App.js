import React, { Component } from "react";


import { Container } from "reactstrap";

import AppNavbar from "./components/AppNavBar/AppNavbar";
import HomeScreen from "./components/HomeScreen/HomeScreen";

// const { Map: LeafletMap, TileLayer, Marker, Popup } = ReactLeaflet



class App extends Component {

  

  render() {
    return (
      <Container
        fluid
        style={style.contsiner}
      >
        <AppNavbar />
        <HomeScreen />
      </Container>
    );
  }
}

const style = {
  contsiner:{
    "paddingRight": "0px",
    "paddingLeft": "0px"
  }
}

export default App;
