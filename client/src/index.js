import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "bootstrap/dist/css/bootstrap.min.css";

// ------------  FIREBASE
import firebase from "firebase/app";
import "firebase/database";
// ------------  REDUX
import { Provider } from "react-redux";
import rootSaga from './store/saga/rootSaga';
import configureStore from "./store/configureStore";

const config = {
  apiKey: "AIzaSyAn58bObEw3dn1s4aXoj-rtI8Kc9QeV3CQ",
  authDomain: "node-react-first.firebaseapp.com",
  databaseURL: "https://node-react-first.firebaseio.com",
  projectId: "node-react-first",
  storageBucket: "node-react-first.appspot.com",
  messagingSenderId: "652795484347"
};

// process.env.MAPQUEST_API_KEY = 2;
firebase.initializeApp(config);

const reduxStore = configureStore();
reduxStore.runSaga(rootSaga);

ReactDOM.render(
  <Provider store={reduxStore}>
    <App />
  </Provider>,
  document.getElementById("root") // eslint-disable-line no-undef
);
