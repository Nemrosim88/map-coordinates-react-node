import L from "leaflet";

const arrowDown = new L.Icon({
  iconUrl: require("./arrow-max-v2.png"),
  shadowUrl: require("./arrow-max-v2-shadow.png"),

  // iconRetinaUrl: require("./arrow-max-v2-shadow.png"),
  iconAnchor: null,
  popupAnchor: null,
  shadowSize: new L.Point(90, 100),
  shadowAnchor: null,
  iconSize: new L.Point(90, 100),
//   className: "leaflet-div-icon"
});

export default arrowDown;
