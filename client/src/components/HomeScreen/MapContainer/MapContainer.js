import React, { Component } from "react";
import { Map, TileLayer, Polyline } from "react-leaflet";
import { Container } from "semantic-ui-react";

// const polyline = [
//   [50.4215814, 30.47915575],
//   [50.4215813, 30.47915576],
//   [50.4215816, 30.47915577]
// ];

export default class MapContainer extends Component<{}, State> {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      zoom: 16
    };
  }

  render() {
    // const divStyle = {
    //   height: "500px",
    //   width: "100%",
    //   class: "container"
    // };

    let polyline = null;
    if (this.props.polylinePositions) {
      polyline = (
        // PolylineOptions: {
        // smoothFactor?: number;
        // noClip?: boolean;
        // stroke?: boolean;
        // color?: string;
        // weight?: number;
        // opacity?: number;
        // lineCap?: LineCapShape;
        // lineJoin?: LineJoinShape;
        // dashArray?: string;
        // dashOffset?: string;
        // fill?: boolean;
        // fillColor?: string;
        // fillOpacity?: number;
        // fillRule?: FillRule;
        // renderer?: Renderer;
        // className?: string;}
        <Polyline
          color={this.props.polylineColor}
          positions={this.props.polylinePositions}
          stroke={true}
        />
      );
    }

    let startPosition = [50.4215814, 30.47915575];
    if (this.props.polylinePositions) {
      startPosition = [
        this.props.polylinePositions[0][0],
        this.props.polylinePositions[0][1]
      ];
    }

    return (
      <Container fluid>
        <Map
          center={startPosition}
          zoom={this.state.zoom}
          style={{ marginTop: "1em", height: "30em", width: "100%" }}
        >
          <TileLayer
            attribution='&amp;copy <a href="//www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          {this.props.markers}
          {polyline}
        </Map>
      </Container>
    );
  }
}
