import React from "react";
import { Marker, Popup } from "react-leaflet";

const MapMarker = props => {
  if (props.icon) {
    return (
      <Marker
        key={props.key}
        position={[props.latitude, props.longitude]}
        icon={props.icon}
      >
        <Popup>
          Longitude: {props.latitude}. <br /> Latitude: {props.longitude}.<br />
          Time: {props.time}.<br /> UserEmail: {props.userEmail}.<br /> UserKey:
          {props.userKey}.
        </Popup>
      </Marker>
    );
  } else {
    return (
      <Marker key={props.key} position={[props.latitude, props.longitude]}>
        <Popup>
          Longitude: {props.latitude}. <br /> Latitude: {props.longitude}.<br />
          Time: {props.time}. <br /> UserEmail: {props.userEmail}.<br />
          UserKey: {props.userKey}.
        </Popup>
      </Marker>
    );
  }
};

export default MapMarker;
