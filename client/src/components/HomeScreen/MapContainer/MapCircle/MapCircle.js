import React from "react";
import { Circle } from "react-leaflet";

const MapCircle = props => {
  return (
    <Circle
      center={{ lat: props.latitude, lng: props.longitude }}
      fillColor="red"
      radius={200}
    />
  );
};

export default MapCircle;
