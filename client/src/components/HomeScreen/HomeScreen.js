import React from "react";
import { Container } from "reactstrap";

import { connect } from "react-redux";

import firebase from "firebase/app";
import "firebase/database";

import CoordinatesTable from "./CoordinatesTable/CoordinatesTable";
import MapContainer from "./MapContainer/MapContainer";
import MapMarker from "./MapContainer/MapMarker/MapMarker";

import CustomMarkerIcon from "./MapContainer/CustomMarkerIcon/CustomMarkerIcon";
import RegistrationModal from "./RegistrationModal/RegistrationModal";
import GetCoordinatesForm from "./CoordinatesTable/GetCoordinatesForm/GetCoordinatesForm";
import CoordinateRow from "./CoordinatesTable/CoordinateRow";

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);

    this.getCoordinatesFromDbHandler = this.getCoordinatesFromDbHandler.bind(
      this
    );
    this.registrationModalToggle = this.registrationModalToggle.bind(this);
  }

  state = {
    rows: null,
    hello: null,
    polylinePositions: null,
    logged: false,
    user: {
      email: "",
      password: "",
      childKey: ""
    },
    loadingCoordinates: false,
    registrationModal: false,
    polylineColor: "lime"
  };

  registrationModalToggle() {
    this.setState({
      loadingCoordinates: false,
      registrationModal: !this.state.registrationModal
    });
  }

  deleteRowHandler(i) {
    let rows = [...this.state.rows];
    rows.splice(i, 1);
    this.setState({
      rows: rows
    });
  }

  /**
   *
   * @param {*} event
   */
  getCoordinatesFromDbHandler(event) {
    event.preventDefault();
    this.setState({ loadingCoordinates: true });

    if (this.props.redux_userLogged) {
      var database = firebase.database().ref("/coordinates");

      const amount = this.props.redux_coordinatesAmount;

      let query = null;
      if (
        this.props.redux_limitAmountOfCoordinates === true &&
        amount !== null
      ) {
        query = database
          .orderByChild("userEmail")
          .equalTo(this.props.redux_userEmail)
          .limitToFirst(Number.parseInt(amount));
      } else {
        query = database
          .orderByChild("userEmail")
          .equalTo(this.props.redux_userEmail);
      }

      const dataFromDb = [];

      query
        .once("value", snapshot => {
          snapshot.forEach(child => {
            let coordinates = {
              latitude: "",
              longitude: "",
              time: "",
              userEmail: "",
              userKey: ""
            };

            const val = child.val();

            coordinates.latitude = val.latitude;
            coordinates.longitude = val.longitude;
            coordinates.time = val.time;
            coordinates.userEmail = val.userEmail;
            coordinates.userKey = val.userKey;
            dataFromDb.push(coordinates);
          });
        })
        .then(result => {
          this.setState({ loadingCoordinates: false });

          this.getCoordinatesHandler(dataFromDb);

          if (this.props.redux_showMarkers === true) {
            this.setMarkers(dataFromDb);
          } else if (this.props.redux_showCustomIcon === true) {
            this.setStartMarker(dataFromDb);
          }
        })
        .catch(error => {
          console.log(error.message);
        });
    } else {
      this.registrationModalToggle();
    }
  }

  /**
   *
   * @param {*} data
   */
  setStartMarker(data) {
    let first = true; // eslint-disable-next-line
    const leafletMarkers = Object.keys(data).map(key => {
      let keyValue = data[key];
      let newArray = [...Array(keyValue)];
      if (first) {
        first = false;
        return newArray.map((_, i) => {
          return (
            <MapMarker
              key={key}
              latitude={data[key].latitude}
              longitude={data[key].longitude}
              time={data[key].time}
              userEmail={data[key].userEmail}
              userKey={data[key].userKey}
              icon={CustomMarkerIcon}
            />
          );
        });
      }
    });
    this.setState({ markers: leafletMarkers });
  }

  /**
   *
   * @param {*} data
   */
  getCoordinatesHandler(data) {
    let sortedCoordinatesForPolylines = [];
    const rows = Object.keys(data).map(key => {
      const idKey = "key" + key;
      let keyValue = data[key];
      let newArray = [...Array(keyValue)];

      return newArray.map((_, i) => {
        sortedCoordinatesForPolylines.push([
          data[key].latitude,
          data[key].longitude
        ]);
        return (
          <CoordinateRow
            keyFrom={idKey}
            latitude={data[key].latitude}
            longitude={data[key].longitude}
            time={data[key].time}
            userEmail={data[key].userEmail}
            userKey={data[key].userKey}
            deleteRowHandler={idKey => this.deleteRowHandler(idKey)}
          />
        );
      });
    });

    if (this.props.redux_showPath === true) {
      this.setState({
        polylinePositions: sortedCoordinatesForPolylines
      });
    }
    this.setState({ rows: rows });
  }

  /**
   * First, this method sorts data from server(DB) by date.
   * Then, if element is first in array it will set not ordinary marker for starting point of the gps path.
   * @param {*} data
   */
  setMarkers(data) {
    let first = true;
    const leafletMarkers = Object.keys(data).map(key => {
      let keyValue = data[key];
      let newArray = [...Array(keyValue)];

      if (
        first &&
        this.props.redux_showCustomIcon === true &&
        this.props.redux_showPath === true
      ) {
        first = false;
        return newArray.map((_, i) => {
          return (
            <MapMarker
              key={key}
              latitude={data[key].latitude}
              longitude={data[key].longitude}
              time={data[key].time}
              userEmail={data[key].userEmail}
              userKey={data[key].userKey}
              icon={CustomMarkerIcon}
            />
          );
        });
      } else {
        return newArray.map((_, i) => {
          return (
            <MapMarker
              key={key}
              latitude={data[key].latitude}
              longitude={data[key].longitude}
              time={data[key].time}
              userEmail={data[key].userEmail}
              userKey={data[key].userKey}
            />
          );
        });
      }
    });

    this.setState({ markers: leafletMarkers });
  }

  render() {
    const loader = (
      <div class="ui segment">
        <div class="ui active inverted dimmer">
          <div class="ui text loader">Loading</div>
        </div>
        <p />
      </div>
    );

    return (
      <Container fluid>
        <MapContainer
          markers={this.state.markers}
          polylinePositions={this.state.polylinePositions}
          polylineColor={this.state.polylineColor}
        />

        <GetCoordinatesForm
          onClick={this.getCoordinatesFromDbHandler}
          loadingCoordinates={this.state.loadingCoordinates}
        />

        {this.state.loadingCoordinates ? (
          loader
        ) : (
          <CoordinatesTable coordinateRows={this.state.rows} />
        )}

        <RegistrationModal
          isOpen={this.state.registrationModal}
          toggle={this.registrationModalToggle}
        />
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    // OPTIONS:
    redux_showFirstAndLast: state.opt.showFirstAndLast,
    redux_showPath: state.opt.showPath,
    redux_showMarkers: state.opt.showMarkers,
    redux_showCustomIcon: state.opt.showCustomIcon,
    redux_limitAmountOfCoordinates: state.opt.limitAmountOfCoordinates,
    redux_coordinatesAmount: state.opt.coordinatesAmount,
    redux_userLogged: state.log.auth,
    redux_userEmail: state.log.email,

    // LOGIN
    auth: state.log.loading
    // coords: state.coordinates
  };
};

// const mapDispatchToProps = dispatch => {
//   return {
//     onRequestUser: (email, password) => {
//       dispatch(requestUser(email, password));
//     }
//   };
// };

//  Функция которая возвращает функцию
//               |
//               |
//               V

export default connect(
  mapStateToProps,
  null
)(HomeScreen);
