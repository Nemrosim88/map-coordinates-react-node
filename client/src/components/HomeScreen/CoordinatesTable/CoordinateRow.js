import React from "react";
import RowModal from "../../RowModal";

class CoordinateRow extends React.Component {
  render() {
    const date = new Date(this.props.time);
    const day = ""
      .concat(date.getFullYear())
      .concat("-")
      .concat(date.getMonth() + 1)
      .concat("-")
      .concat(date.getDay());
    const time = ""
      .concat(date.getHours())
      .concat("-")
      .concat(date.getMinutes())
      .concat("-")
      .concat(date.getSeconds());

    return (
      <tr key={this.props.keyFrom}>
        <th scope="row">
          {this.props.longitude}
          <br />
          {this.props.latitude}
        </th>
        <td>
          {day}
          <br />
          {time}
        </td>
        <td>
          <RowModal
            latitude={this.props.latitude}
            longitude={this.props.longitude}
            time={this.props.time}
            userEmail={this.props.userEmail}
            userKey={this.props.userKey}
          />
        </td>
        <td>
          <button
            type="button"
            className="btn btn-outline-danger"
            onClick={this.props.deleteRowHandler}
          >
            <i className="far fa-trash-alt" />
          </button>
        </td>
      </tr>
    );
  }
}

export default CoordinateRow;
