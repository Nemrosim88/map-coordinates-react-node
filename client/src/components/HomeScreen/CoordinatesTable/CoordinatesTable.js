import React from "react";
import { Container, Row, Table } from "reactstrap";

const GetCoordinatesFromDB = (props) => {
  return (
    <Container>
      <Row style={{"justifyContent":"center", "marginBottom":"2em"}}>
        <h4>Results</h4>
      </Row>
      <Table>
        <thead>
          <tr>
            <th>Coordinates</th>
            <th>Time</th>
            <th>Info</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>{props.coordinateRows}</tbody>
      </Table>
    </Container>
  );
};

export default GetCoordinatesFromDB;
