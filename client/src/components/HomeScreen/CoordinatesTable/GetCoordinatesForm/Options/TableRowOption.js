import React from "react";
import { Header, Table } from "semantic-ui-react";
import "semantic-ui-css/semantic.min.css";

import AppCheckbox from "../ChooseButton/AppCheckbox";

const TableRowOption = props => {
  return (
    <Table.Row>
      <Table.Cell>
        <Header.Content>
          <h4> {props.headerContent} </h4>
        </Header.Content>
      </Table.Cell>
      <Table.Cell>
        <AppCheckbox onChange={props.onChange}/>
      </Table.Cell>
    </Table.Row>
  );
};

export default TableRowOption;
