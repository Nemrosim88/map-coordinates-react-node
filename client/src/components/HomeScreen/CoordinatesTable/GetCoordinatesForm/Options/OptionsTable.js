import React from "react";
import { Table } from "semantic-ui-react";
import { connect } from "react-redux";
import "semantic-ui-css/semantic.min.css";

//REDUX IMPORTS
import { redux_showMarkerChange } from "../../../../../store/reducers/options";
import { redux_showFirsAndLast } from "../../../../../store/reducers/options";
import { redux_showPath } from "../../../../../store/reducers/options";
import { redux_showCustomIcon } from "../../../../../store/reducers/options";
import { redux_limitAmountOfCoordinates } from "../../../../../store/reducers/options";
import { redux_amountChanged } from "../../../../../store/reducers/options";

// COMPONENTS IMPORT
import TableRowOption from "./TableRowOption";
import TableRowOptionWithInput from "./TableRowOptionWithInput";

class OptionsTable extends React.Component {
  render() {
    return (
      <Table style={{ border: "0px" }}>
        <Table.Body>
          <TableRowOption
            key={"onlyFirstAndLast"}
            headerContent={"Show only first and last coordinates"}
            onChange={e => this.props.showFirsAndLast(e)}
          />
          <TableRowOption
            key={"showMarkers"}
            headerContent={"Show markers"}
            onChange={e => this.props.showMarkerChange(e)}
          />
          <TableRowOption
            key={"showPath"}
            headerContent={"Show path"}
            onChange={e => this.props.showPath(e)}
          />
          <TableRowOption
            key={"showCustomIcon"}
            headerContent={"Show custom icon for the start path"}
            onChange={e => this.props.showCustomIcon(e)}
          />
          <TableRowOption
            key={"limitAmountOfCoordinates"}
            headerContent={"Limit amount of coordinates"}
            onChange={e => this.props.limitAmountOfCoordinates(e)}
          />
          <TableRowOptionWithInput
            key={"coordinatesAmount"}
            headerContent={"Amount of coordinates"}
            onChange={e => this.props.amountChanged(e)}
          />
        </Table.Body>
      </Table>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    showMarkerChange: event =>
      dispatch(redux_showMarkerChange(event.target.checked)),
    showFirsAndLast: event =>
      dispatch(redux_showFirsAndLast(event.target.checked)),
    showPath: event => dispatch(redux_showPath(event.target.checked)),
    showCustomIcon: event =>
      dispatch(redux_showCustomIcon(event.target.checked)),
    limitAmountOfCoordinates: event =>
      dispatch(redux_limitAmountOfCoordinates(event.target.checked)),
    amountChanged: event => dispatch(redux_amountChanged(event.target.value))
  };
};

export default connect(
  null,
  mapDispatchToProps
)(OptionsTable);
