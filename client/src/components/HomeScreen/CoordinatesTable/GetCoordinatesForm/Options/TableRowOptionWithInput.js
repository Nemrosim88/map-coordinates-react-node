import React from "react";
import { Table, Input } from "semantic-ui-react";
import "semantic-ui-css/semantic.min.css";


const TableRowOptionWithInput = props => {
  return (
    <Table.Row>
      <Table.Cell>
        <Input
          type="number"
          name="amount"
          id="coordAmount"
          placeholder="How many coordinates to show"
          onChange={props.onChange}
          style={{width:"100%"}}
        />
      </Table.Cell>
    </Table.Row>
  );
};

export default TableRowOptionWithInput;
