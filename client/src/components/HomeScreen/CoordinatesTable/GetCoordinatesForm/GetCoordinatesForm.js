import React from "react";
import { Form } from "reactstrap";

import { Container, Header, Button } from "semantic-ui-react";
import "semantic-ui-css/semantic.min.css";

import OptionsTable from "./Options/OptionsTable";
import "./GetCoordinatesForm.scss";

const GetCoordinatesForm = props => {
  const loadingButton = (
    <Button id="loadingButton" loading color="green" fluid>
      <h4>Loading</h4>
    </Button>
  );

  const notLoadingButton = (
    <Button id="loadingButton" color="green" fluid onClick={props.onClick}>
      <h4>Get coordinates</h4>
    </Button>
  );

  return (
    <Container>
      <Header
        id="coordinateOptionsHeader"
        as="h2"
        content="Options"
        textAlign="center"
      />

      <OptionsTable />

      <Form>
        <Container>
          {props.loadingCoordinates ? loadingButton : notLoadingButton}
        </Container>
      </Form>
    </Container>
  );
};

export default GetCoordinatesForm;
