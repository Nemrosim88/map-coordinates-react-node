import React from "react";
import "semantic-ui-css/semantic.min.css";

class ChooseButton extends React.Component {
  render() {
    return (
      <div className="ui toggle checkbox">
        <input type="checkbox" name="public" onChange={this.props.onChange}/>
        <label>No/Yes</label>
      </div>
    );
  }
}

export default ChooseButton;
