import React from "react";
import { Button } from "semantic-ui-react";

class ChooseButton extends React.Component {
  render() {
    return (
      <Button.Group size="large">
        <Button>Yes</Button>
        <Button.Or />
        <Button>No</Button>
      </Button.Group>
    );
  }
}

export default ChooseButton;
