import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";

const RegistrationModal = props => {
  return (
    <Modal isOpen={props.isOpen} toggle={props.toggle}>
      <ModalHeader toggle={props.toggle}>Registration</ModalHeader>
      <ModalBody>Please, login.</ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={props.toggle} block>
          Ok
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default RegistrationModal;
