import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import axios from "axios";

import { Dimmer, Loader, Image, Segment } from "semantic-ui-react";
import 'semantic-ui-css/semantic.min.css';

const AppLoader = () => (
    <Segment>
      <Dimmer active inverted>
        <Loader inverted content="Loading" />
      </Dimmer>

      <Image src='https://react.semantic-ui.com/images/wireframe/short-paragraph.png' />
    </Segment>
);



class RowModal extends React.Component {
  constructor(props) {
    super(props);

    this.loadInfo = this.loadInfo.bind(this);
    this.state = {
      modal: false,
      url: "",
      postalCode: "",
      city: "",
      street: "",
      loading: true
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.loadInfo();
    this.setState({
      modal: !this.state.modal
    });
  }

  loadInfo() {
    console.log("LOADING...");
    const location = `${this.props.latitude},${this.props.longitude}`;
    const key = "aVFEAdnOwsdewQCr4iiAECG3h1EDsG29";
    const uri = "https://www.mapquestapi.com/geocoding/v1/reverse";
    const params = "includeRoadMetadata=true&includeNearestIntersection=true";

    axios
      .get(`${uri}?key=${key}&location=${location}&${params}`)
      .then(response => {
        this.setState({
          city: response.data.results[0].locations[0].adminArea5,
          postalCode: response.data.results[0].locations[0].postalCode,
          street: response.data.results[0].locations[0].street,
          loading: false
        });
      })
      .catch(error => {
        console.log(error.message);
      });
  }

  render() {
    const info = (
      <div>
        <br /> PostalCode:
        {this.state.postalCode}
        <br /> City: {this.state.city}
        <br /> Street: {this.state.street}
      </div>
    );
    return (
      <div>
        <Button
          color="danger"
          className="btn btn-outline-info"
          onClick={this.toggle}
        >
          <i className="fas fa-question" />
        </Button>
        <Modal
          isOpen={this.state.modal}
          toggle={this.toggle}
          className={this.props.className}
        >
          <ModalHeader toggle={this.toggle}>Detailed Information</ModalHeader>
          <ModalBody>
            Longitude: {this.props.latitude}. <br /> Latitude:
            {this.props.longitude}.<br /> Time: {this.props.time}.<br />
            UserEmail: {this.props.userEmail}.<br /> UserKey:
            {this.props.userKey} <br/>
            {this.state.loading ? <AppLoader /> : info}
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.toggle}>
              Do Something
            </Button>{" "}
            <Button color="secondary" onClick={this.toggle}>
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default RowModal;
