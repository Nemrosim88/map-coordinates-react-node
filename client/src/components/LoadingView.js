import React from "react";
import "semantic-ui-css/semantic.min.css";

const LoadingView = props => {
  return (
    <div class="ui segment">
      <div class="ui active inverted dimmer">
        <div class="ui text loader">Loading</div>
      </div>
      <p />
    </div>
  );
};

export default LoadingView;

