import React from "react";
import { Form, Label, FormGroup, Col, Button } from "reactstrap";
import { connect } from "react-redux";
import { logoutUser } from "../../../store/reducers/login";

class LogOutForm extends React.Component {
  constructor(props) {
    super(props);
    this.logoutHandler = this.logoutHandler.bind(this);
  }

  logoutHandler() {
    // sessionStorage.removeItem("user");

    this.forceUpdate();
  }

  render() {
    return (
      <Form>
        <FormGroup row>
          <Label sm={2}>You logged as: {this.props.reduxUserEmail}</Label>
          <Col sm={10}>
            <Button onClick={this.props.reduxLogoutUser} block={true} color="warning">
              LogOut
            </Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

const mapStateToProps = state => {
  return {
    userLogged: state.log.auth,
    reduxUserEmail: state.log.email
  };
};

const mapDispatchToProps = dispatch => {
  return {
    reduxLogoutUser: () => {
      dispatch(logoutUser());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LogOutForm);
