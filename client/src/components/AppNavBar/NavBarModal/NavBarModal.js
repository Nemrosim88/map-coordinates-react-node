import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";

const NavBarModal = props => {
  return (
    <Modal
      isOpen={props.isOpen}
      toggle={props.toggle}
    >
      <ModalHeader toggle={props.toggle}>
        <h5 style={{ color: "red" }}>{props.headerText}</h5>
      </ModalHeader>
      <ModalBody>{props.text}</ModalBody>
      <ModalFooter>
        <Button
          color={props.color}
          onClick={props.toggle}
          block
        >
          Got it
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default NavBarModal;
