import React from "react";
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Form,
  FormGroup,
  Label,
  Col,
  Input
} from "reactstrap";
import axios from "axios";

export default class UIDropdown extends React.Component {
  constructor(props) {
    super(props);

    this.onChangeHandler = this.onChangeHandler.bind(this);

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false,
      value: "",
      results: null,
      dropdownList: <DropdownItem>Nothing</DropdownItem>
    };
  }

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  onChangeHandler({ target: { value } }) {
    this.setState(() => ({ value }));
    this.setState({ dropdownOpen: true });

    // const list = this.state.results;
    // list.push(value);
    // this.setState(list);

    axios
      // https://node-react-first.firebaseio.com/coordinates.json?orderBy="time"&startAt="${value}"&print="pretty"
      .get(
        `https://node-react-first.firebaseio.com/users.json?orderBy="name"&startAt="${value}"&limitToFirst=5`
      )
      .then(response => {
        this.setState({ results: response.data });

        const list = Object.keys(response.data)
          // .slice(0, 3)
          .map(key => {
            let keyValue = response.data[key];
            let newArray = [...Array(keyValue)];
            return newArray.map((_, i) => {
              return (
                <DropdownItem>
                  Name:
                  {response.data[key].name}             
                  Long:
                  {response.data[key].longitude} 
                  Lat:
                  {response.data[key].latitude} 
                  Time:
                  {response.data[key].time} 
                </DropdownItem>
              );
            });
          });

          this.setState({dropdownList:list})
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    return (
      <Form>
        <FormGroup row>
          <Label for="exampleEmail" sm={2}>
            Search (Demo: Artem, Andrey)
          </Label>
          <Col sm={10}>
            <Input
              type="text"
              name="search"
              id="searchID"
              placeholder="Поиск по имени (Demo)"
              value={this.state.value}
              onChange={e => this.onChangeHandler(e)}
            />
            <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
              <DropdownToggle
                tag="span"
                onClick={this.toggle}
                data-toggle="dropdown"
                aria-expanded={this.state.dropdownOpen}
              />
              <DropdownMenu>
                {/* <ListGroup flush>
        <ListGroupItem disabled tag="a" href="#">Cras justo odio</ListGroupItem>
        <ListGroupItem tag="a" href="#">Dapibus ac facilisis in</ListGroupItem>
        <ListGroupItem tag="a" href="#">Morbi leo risus</ListGroupItem>
        <ListGroupItem tag="a" href="#">Porta ac consectetur ac</ListGroupItem>
        <ListGroupItem tag="a" href="#">Vestibulum at eros</ListGroupItem>
      </ListGroup> */}
                {this.state.dropdownList}
              </DropdownMenu>
            </Dropdown>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}
