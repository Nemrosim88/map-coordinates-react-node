import React from 'react'
import { Loader } from 'semantic-ui-react'
import 'semantic-ui-css'

const UILoader = () => <Loader active inline />

export default UILoader