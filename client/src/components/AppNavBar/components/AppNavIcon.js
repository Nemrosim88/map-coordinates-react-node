import React from "react";
import { NavbarBrand } from "reactstrap";
import { Image } from "semantic-ui-react";

const AppNavIcon = props => {
  return (
    <NavbarBrand href="/" className="mr-auto">
      <Image
        src="../images/placeholder.png"
        avatar
        style={{ height: "40px", width: "40px" }}
      />
      MapApp
    </NavbarBrand>
  );
};

export default AppNavIcon;
