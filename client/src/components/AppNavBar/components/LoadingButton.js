import React from "react";
import { Button } from "semantic-ui-react";

const LoadingButton = () => (
  <Button
    style={{
      borderRadius: "25px",
      width: "80%",
      alignSelf: "center",
      marginLeft: "10%",
      marginRight: "10%",
      color: "green",
      backgroundColor: "green"
    }}
    basic
    loading
  >
    Loading
  </Button>
);

export default LoadingButton;
