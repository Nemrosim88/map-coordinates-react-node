import React from "react";
import { Collapse, Navbar, NavbarToggler, Nav, NavItem } from "reactstrap";
import { connect } from "react-redux";

import AppNavIcon from "./components/AppNavIcon";
import LoginForm from "./LoginForm/LoginForm";
import LogOutForm from "./LogOutForm/LogOutForm";

class AppNavbar extends React.Component {
  constructor(props) {
    super(props);

    this.toggleNavbar = this.toggleNavbar.bind(this);
    this.logoutHandler = this.logoutHandler.bind(this);

    this.state = {
      navbarModal: true,
      user: {
        email: null,
        password: null,
        childKey: null
      },
    };
  }

  toggleNavbar() {
    this.setState({
      navbarModal: !this.state.navbarModal
    });
  }

  // componentWillMount() {
  //   let sessionUser = sessionStorage.getItem("user");
  //   if (sessionUser === null || sessionUser === undefined) {
  //     this.setState({ logged: false });
  //   }
  // }

  logoutHandler() {
    sessionStorage.removeItem("user");
    this.setState({ state: this.state });
    this.forceUpdate();
  }

  render() {   

    const logoutForm = (
      <LogOutForm
        logoutHandler={this.logoutHandler}
      />
    );

    return (
      <Navbar color="light" light>
        <AppNavIcon />
        <NavbarToggler onClick={this.toggleNavbar} className="mr-1" />
        <Collapse isOpen={!this.state.navbarModal} navbar>
          <Nav navbar>
            <NavItem>
              {this.props.userLogged ? logoutForm : <LoginForm />}
            </NavItem>
            <NavItem>{/* <UIDropdown /> */}</NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    );
  }
}

const mapStateToProps = state => {
  return {
    userLogged: state.log.auth,
  };
};

export default connect(
  mapStateToProps,
  null
)(AppNavbar);
