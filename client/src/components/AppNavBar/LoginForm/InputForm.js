import React from "react";
import { Label, FormGroup, Col, Input } from "reactstrap";

const InputForm = props => {
  return (
    <FormGroup row>
      <Label sm={2}>
        <h4>{props.text}</h4>
      </Label> 
      <Col sm={10}>
        <Input
          type="email"
          name="email"
          id="exampleEmail"
          placeholder="Email"
          onChange={props.onChange}
        />
      </Col>
    </FormGroup>
  );
};

export default InputForm;
