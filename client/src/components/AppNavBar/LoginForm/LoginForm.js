import React from "react";
import { Form, Label, FormGroup, Col, Input } from "reactstrap";
import { connect } from "react-redux";

import { requestUser } from "../../../store/reducers/login";

import LoadingButton from "../components/LoadingButton";
import NavBarModal from "./../NavBarModal/NavBarModal";
import LoginButton from "./LoginButton";
import InputForm from "./InputForm";

class LoginForm extends React.Component {
  constructor(props) {
    super(props);

    this.toggleLoginModal = this.toggleLoginModal.bind(this);
    this.togglePasswordModal = this.togglePasswordModal.bind(this);

    this.togglePasswordDidNotMatch = this.togglePasswordDidNotMatch.bind(this);
    this.toggleEmailDidNotMatch = this.toggleEmailDidNotMatch.bind(this);

    this.loginButtonOnClickHandler = this.loginButtonOnClickHandler.bind(this);

    this.onEmailInputHandler = this.onEmailInputHandler.bind(this);
    this.onPasswordInputHandler = this.onPasswordInputHandler.bind(this);

    this.state = {
      loginModal: false,
      passwordModal: false,

      passwordDidNotMatchModal: true,
      emailDidNotMatchModal: true,

      emailFromInput: "",
      passwordFromInput: ""
    };
  }

  onEmailInputHandler(event) {
    this.setState({ emailFromInput: event.target.value });
  }

  onPasswordInputHandler(event) {
    this.setState({ passwordFromInput: event.target.value });
  }

  /**
   *
   */
  toggleLoginModal() {
    this.setState({
      loginModal: !this.state.loginModal
    });
  }

  /**
   *
   */
  togglePasswordModal() {
    this.setState({
      passwordModal: !this.state.passwordModal
    });
  }

  /**
   *
   */
  togglePasswordDidNotMatch() {
    this.setState({
      passwordDidNotMatchModal: !this.state.passwordDidNotMatchModal
    });
  }

  /**
   *
   */
  toggleEmailDidNotMatch() {
    this.setState({
      emailDidNotMatchModal: !this.state.emailDidNotMatchModal
    });
  }

  /**
   *
   * @param {*} event
   */
  loginButtonOnClickHandler(event) {
    if (this.state.emailFromInput === "") {
      console.log("[LoginForm] Email not entered");
      this.toggleLoginModal();
    } else if (this.state.passwordFromInput === "") {
      console.log("[LoginForm] Password not entered");
      this.togglePasswordModal();
    } else {
      console.log("[LoginForm] Password entered");

      console.log("BEFORE", this.props.edm);

      this.props.onRequestUser(
        this.state.emailFromInput,
        this.state.passwordFromInput
      );

      console.log("AFTER", this.props.edm);
    }
  }

  render() {
    return (
      //   <Form onSubmit={this.props.onSubmitButtonHandler}>
      <Form>
        <InputForm
          text={"Email"}
          onChange={event => this.onEmailInputHandler(event)}
        />

        <FormGroup row>
          <Label for="password" sm={2}>
            <h4> Password</h4>
          </Label>
          <Col sm={10}>
            <Input
              type="password"
              name="password"
              id="examplePassword"
              placeholder="Password"
              onChange={event => this.onPasswordInputHandler(event)}
            />
          </Col>
        </FormGroup>
        <FormGroup row>
          {this.props.loadinUser ? (
            <LoadingButton />
          ) : (
            <LoginButton onClick={e => this.loginButtonOnClickHandler(e)} />
          )}
        </FormGroup>

        <NavBarModal
          isOpen={this.state.loginModal}
          toggle={this.toggleLoginModal}
          headerText={"Login Information"}
          text={"Please, input email"}
          color={"info"}
        />
        <NavBarModal
          isOpen={this.state.passwordModal}
          toggle={this.togglePasswordModal}
          headerText={"Password Information"}
          text={"Please, input password"}
          color={"info"}
        />

        {this.props.pdm ? (
          <NavBarModal
            isOpen={this.state.passwordDidNotMatchModal}
            toggle={this.togglePasswordDidNotMatch}
            headerText={"Warning!"}
            text={"Password did not match"}
            color={"danger"}
          />
        ) : null}

        {this.props.edm ? (
          <NavBarModal
            isOpen={this.state.emailDidNotMatchModal}
            toggle={this.toggleEmailDidNotMatch}
            headerText={"Warning!"}
            text={"Entered email is incorrect"}
            color={"danger"}
          />
        ) : null}
      </Form>
    );
  }
}

const mapStateToProps = state => {
  return {
    loadinUser: state.log.loading,
    edm: state.log.emailDontMatch,
    pdm: state.log.passwordDontMatch
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onRequestUser: (email, password) => {
      dispatch(requestUser(email, password));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginForm);
