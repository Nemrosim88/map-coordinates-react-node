import React from "react";
import { Button } from "reactstrap";

const style = {
    button: {
      borderRadius: "25px",
      width: "80%",
      alignSelf: "center",
      marginLeft: "10%",
      marginRight: "10%"
    }
  };

const LoginButton = props => {
  return (
    <Button
    block={true}
    color="success"
    style={style.button}
    onClick={props.onClick}
  >
    <h4>Login</h4>
  </Button>
  );
};

export default LoginButton;
