import { put, takeEvery, all, call } from "redux-saga/effects";

import firebase from "firebase/app";
import "firebase/database";

import * as types from "../constants/login";

export function* requestUserAsync(action) {
  const email = action.email;
  const password = action.password;

  try {
    const data = yield call(() => {
      var database = firebase.database().ref("/users");
      var query = database
        .orderByChild("credentials/email")
        .equalTo(action.email);
      /**
       * Getting data from query and setting values to variables to save in AsyncStorage.
       */
      return query
        .once("value")
        .then(result => {
          let user = {
            email: null,
            password: null,
            childKey: null
          };
          // this.setState({ result: snapshot._value });
          result.forEach(child => {
            user.email = child.val().credentials.email;
            user.childKey = child.key;
            user.password = child.val().credentials.password;
          });

          return user;
        })
        .catch(error => {
          console.log("[saga/login.js] Error: ", error);
        });
    });

    if (data.email !== email) {
      yield put({
        type: types.REQUESTED_USER_FAILED,
        errorObject: null,
        error: true,
        emailDontMatch: true
      });
    } else if (data.password !== password) {
      yield put({
        type: types.REQUESTED_USER_FAILED,
        errorObject: null,
        error: true,
        passwordDontMatch: true
      });
    } else {
      yield put({
        type: types.REQUESTED_USER_SUCCEEDED,
        email: data.email,
        password: data.password,
        childKey: data.childKey
      });
    }
    console.log("HELLO");
  } catch (error) {
    console.log("ERROR: ", error);
    yield put({ type: types.REQUESTED_USER_FAILED, errorObject: error });
  }
}

/**
 * Watchig for login events in LoginForm.js file.
 */
export function* watchRequestUser() {
  yield takeEvery(types.REQUESTING_USER, requestUserAsync);
}

export default function* loginSaga() {
  yield all([watchRequestUser()]);
}
