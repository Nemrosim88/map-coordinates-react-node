import { all } from "redux-saga/effects";

import loginSaga from './login'

/**
 * Combinig saga
 */
export default function* rootSaga() {
    yield all([loginSaga()]);
  }