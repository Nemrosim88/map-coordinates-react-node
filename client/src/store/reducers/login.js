import * as types from "../constants/login";

const initialState = {
  email: "",
  password: "",
  childKey: "",
  emailDontMatch: false,
  passwordDontMatch: false,
  auth: false,
  loading: false,
  error: false,
  errorObject: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.REQUESTING_USER: {
      return {
        ...state,
        loading: true
      };
    }
    case types.REQUESTED_USER_SUCCEEDED: {
      return {
        ...state,
        email: action.email,
        password: action.password,
        childKey: action.childKey,
        emailDontMatch: false,
        passwordDontMatch: false,
        auth: true,
        loading: false
      };
    }
    case types.REQUESTED_USER_FAILED: {
      return {
        ...state,
        error: true,
        emailDontMatch: action.emailDontMatch,
        passwordDontMatch: action.passwordDontMatch,
        errorObject: action.errorObject,
        loading: false
      };
    }
    case types.LOGOUT_USER: {
      return {
        email: "",
        password: "",
        childKey: "",
        emailDontMatch: false,
        passwordDontMatch: false,
        auth: false,
        loading: false,
        error: false,
        errorObject: null
      };
    }
    default:
      return state;
  }
};

/**
 * 
 * @param {*} email 
 * @param {*} password 
 */
export const requestUser = (email, password) => {
  return {
    type: types.REQUESTING_USER,
    email: email,
    password: password
  };
};

/**
 * 
 */
export const logoutUser = () => {
  return {
    type: types.LOGOUT_USER,   
  };
};

export default reducer;
