import * as types from '../constants/options';

const initialState = {
  showFirstAndLast: false,
  showPath: false,
  showMarkers: false,
  showCustomIcon: false,
  limitAmountOfCoordinates: false,
  coordinatesAmount: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SHOW_MARKERS_CHANGED: {
      return {
        ...state,
        showMarkers: action.value
      };
    }

    // +
    case types.SHOW_FIRST_AND_LAST_CHANGED: {
      return {
        ...state,
        showFirstAndLast: action.value
      };
    }
    // +
    case types.SHOW_PATH_CHANGED: {
      return {
        ...state,
        showPath: action.value
      };
    }
    //+
    case types.SHOW_CUSTOM_ICON_CHANGED: {
      return {
        ...state,
        showCustomIcon: action.value
      };
    }
    // +
    case types.LIMIT_AMOUNT_OF_COORDINATES_CHANGED: {
      return {
        ...state,
        limitAmountOfCoordinates: action.value
      };
    }
    case types.AMOUNT_CHANGED: {
      return {
        ...state,
        coordinatesAmount: action.value
      };
    }
    default:
      return state;
  }
};

// ACTION CREATORS


/**
 * Used in OptionsTable.js file. Value true or false, depending of what user has choosed.
 * @param {*} val 
 */
export const redux_showMarkerChange = val => {
  return {
    type: types.SHOW_MARKERS_CHANGED,
    value: val
  };
};

/**
 * Used in OptionsTable.js file. Value true or false, depending of what user has choosed.
 * @param {*} val 
 */
export const redux_showFirsAndLast = val => {
  return {
    type: types.SHOW_FIRST_AND_LAST_CHANGED,
    value: val
  };
};

/**
 * Used in OptionsTable.js file. Value true or false, depending of what user has choosed.
 * @param {*} val 
 */
export const redux_showPath = val => {
  return {
    type: types.SHOW_PATH_CHANGED,
    value: val
  };
};

/**
 * Used in OptionsTable.js file. Value true or false, depending of what user has choosed.
 * @param {*} val 
 */
export const redux_showCustomIcon = val => {
  return {
    type: types.SHOW_CUSTOM_ICON_CHANGED,
    value: val
  };
};

/**
 * Used in OptionsTable.js file. Value true or false, depending of what user has choosed.
 * @param {*} val 
 */
export const redux_limitAmountOfCoordinates = val => {
  return {
    type: types.LIMIT_AMOUNT_OF_COORDINATES_CHANGED,
    value: val
  };
};

/**
 * Used in OptionsTable.js file. Value is integer.
 * @param {*} amount how many coordinates (markers) to show on the map.
 */
export const redux_amountChanged = amount => {
  return {
    type: types.AMOUNT_CHANGED,
    value: amount
  };
};

export default reducer;
