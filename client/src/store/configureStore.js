import { createStore, applyMiddleware, combineReducers, compose } from "redux";
import createSagaMiddleware from "redux-saga";
// REDUCERS
import optionsReducer from "./reducers/options";
import loginReducer from './reducers/login'

// 2. Configuring redux-persist ----
import {persistReducer} from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2'
const persistConfig = {
  key: 'root',
  storage: storage,
  blacklist: ['log'],
  stateReconciler: autoMergeLevel2 // see "Merge Process" section for details.
 };

// ---------------------------------------



export default function configureStore() {
  const rootReducer = combineReducers({
    log: loginReducer,
    opt: optionsReducer
  });

  const pReducer = persistReducer(persistConfig, rootReducer);

  const sagaMiddleware = createSagaMiddleware();
  //FOR REDUX DEV TOOLS in GOOGLE CHROME
  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  return {
    ...createStore(
      pReducer,
      composeEnhancers(applyMiddleware(sagaMiddleware))
    ),
    runSaga: sagaMiddleware.run
  };
}
