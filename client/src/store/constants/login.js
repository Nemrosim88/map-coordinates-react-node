/**
 * Watching by Saga in saga/login.js file.
 */
export const REQUESTING_USER = "REQUESTING_USER";
export const REQUESTED_USER_SUCCEEDED = "REQUESTED_USER_SUCCEEDED";
export const REQUESTED_USER_FAILED = "REQUESTED_USER_FAILED";


export const LOGOUT_USER = "LOGOUT_USER";
export const LOGOUT_USER_SUCCEEDED = "LOGOUT_USER_SUCCEEDED";
export const LOGOUT_USER_FAILED = "LOGOUT_USER_FAILED";






